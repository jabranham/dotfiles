{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./desktop.nix
    ];
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.systemd-boot.editor = false;

  networking.hostName = "earth"; # Define your hostname.
  networking.networkmanager.enable = true;
  networking.networkmanager.wifi.backend = "iwd";
  services.xserver.dpi = 200;

  services.borgbackup.jobs = {
    alexBackup = {
      paths = "/home/alex";
      exclude = [ "*/.cache" ];
      repo = "/home/hdd/borg";
      encryption.mode = "none";
      startAt = "daily";
    };
  };

  systemd.timers.borgbackup-job-alexBackup.timerConfig.Persistent = true;
  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.09"; # Did you read the comment?

}
