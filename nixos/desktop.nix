{ config, pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
in {
  boot.loader.timeout = 2;

  boot.tmpOnTmpfs = true;
  time.timeZone = "America/New_York";

  sound.enable = true;
  hardware.pulseaudio= {
    enable = true;
    support32Bit = true;
  };
  hardware.cpu.intel.updateMicrocode = true;
  hardware.opengl.driSupport32Bit = true;

  nix = {
    autoOptimiseStore = true;
    gc.automatic = true;
    gc.dates = "weekly";
    gc.options = "--delete-older-than 30d";
    trustedUsers = [ "root" "alex" ];
  };

  # all about fonts
  fonts.fontconfig.defaultFonts = {
    monospace = [ "Source Code Pro" ];
    sansSerif = [ "Ubuntu Regular" ];
    serif = [ "Ubuntu Serif" ];
  };
  fonts.fonts = with pkgs; [
    dejavu_fonts
    fira
    font-awesome_5
    libertine
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
    noto-fonts-extra
    source-code-pro
    ubuntu_font_family
  ];

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    layout = "us";
    # Disable the touchpad. For some reason with enable = false I can still click?
    libinput = {
      enable = true;
      touchpad.tapping = false;
    };
    # Have a backup DE ready
    desktopManager.xfce.enable = true;
    desktopManager.xterm.enable = false;
  };
  services.syncthing = {
    enable = true;
    user = "alex";
    openDefaultPorts = true;
    dataDir = "/home/alex/.config/syncthing";
  };
  services.fstrim.enable = true;
  services.openssh = {
    enable = true;
    passwordAuthentication = false;
    challengeResponseAuthentication = false;
  };
  programs.gnupg = {
    agent = {
      enable = true;
      enableSSHSupport = true;
    };
  };
  environment.homeBinInPath = true;
  users.users.alex = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networking" "audio" ];
    uid = 1000;
    openssh.authorizedKeys.keys = [
      (import ./files/gpg-pubkey.nix)
    ];
  };
  users.users.kyle = {
    isNormalUser = true;
    extraGroups = [ "networking" "audio" ];
  };
  users.users.games = {
    isNormalUser = true;
    extraGroups = [ "audio" ];
  };
  services.dovecot2 = {
    enable = true;
    mailLocation = "maildir:~/.mail/:LAYOUT=fs:INBOX=/var/spool/mail/%u";
  };
  networking.firewall = {
    allowedTCPPorts = [ 27036 27037 ];
    allowedUDPPorts = [ 27031 27036 4380 ];
    allowedUDPPortRanges = [
      { from = 27000; to = 27015; }
      { from = 27015; to = 27030; }
    ];
  };
}
