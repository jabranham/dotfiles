self: super:
{
  myPython = super.python3.withPackages (ps: with ps; [
    flake8
    numpy
    pandas
    pandas-datareader
    python-lsp-server
  ]);
}
