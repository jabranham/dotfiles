self: super:

{
  myR = super.rWrapper.override {
    packages = with self.rPackages; [
      data_table
      ggplot2
      languageserver
      lintr
      quantmod
      roxygen2
      rvest
    ];
  };
}
