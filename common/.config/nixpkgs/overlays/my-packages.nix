self: super:

{
  myProfile = super.writeText "my-profile" ''
export PATH=$HOME/.nix-profile/bin:/nix/var/nix/profiles/default/bin:/sbin:/bin:/usr/sbin:/usr/bin
export MANPATH=$HOME/.nix-profile/share/man:/nix/var/nix/profiles/default/share/man:/usr/share/man
export INFOPATH=$HOME/.nix-profile/share/info:/nix/var/nix/profiles/default/share/info:/usr/share/info
    '';
  my-packages = super.buildEnv {
    name = "my-packages";
    paths = [
      (super.runCommand "profile" {} ''
mkdir -p $out/etc/profile.d
cp ${self.myProfile} $out/etc/profile.d/my-profile.sh
        '')
      self.aspell
      self.aspellDicts.en
      self.cachix
      self.dunst
      self.emacsGcc
      self.firefox
      self.git
      self.gnumake
      self.i3lock
      self.isync
      self.ledger
      self.libnotify
      self.networkmanagerapplet
      self.pass
      self.polybar
      self.poppler_utils
      self.myPython
      self.myR
      self.redshift
      self.scrot
      self.signal-desktop
      self.sqlite
      self.stow
      self.texinfoInteractive
      self.texlive.combined.scheme-full
      self.trash-cli
      self.udiskie
      self.wget
      self.xfce.xfce4_power_manager_gtk3
    ];
    extraOutputsToInstall = [ "man" "doc" "info" ];
    postBuild = ''
      if [ -x $out/bin/install-info -a -w $out/share/info ]; then
        shopt -s nullglob
        for i in $out/share/info/*.info $out/share/info/*.info.gz; do
          $out/bin/install-info $i $out/share/info/dir
        done
      fi
      '';
  };
}
